package nl.jvdkolk.selenium.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.MalformedURLException;
import java.net.URL;

@Configuration
public class WebDriverConfig {

    @Value("${webdriver.location}")
    private String webdriverHost;

    @Bean(destroyMethod = "close")
    public WebDriver webDriver() throws MalformedURLException {
        final URL host = new URL(String.format("http://%s/wd/hub", webdriverHost));
        return new RemoteWebDriver(host, new ChromeOptions());
    }
}
