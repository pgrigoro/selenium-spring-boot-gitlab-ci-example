package nl.jvdkolk.selenium;

import nl.jvdkolk.selenium.utils.IpHelper;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SeleniumApplicationTests {

    @Autowired
    private WebDriver webDriver;

    @LocalServerPort
    private int port;

    @Test
    void contextLoads() {
        final String applicationUrl = String.format("http://%s:%s", IpHelper.getPublicApplicationIp(), port);

        webDriver.get(applicationUrl);

        assertThat(webDriver.getPageSource()).isEqualTo("<html><head></head><body>hello</body></html>");
    }

}
