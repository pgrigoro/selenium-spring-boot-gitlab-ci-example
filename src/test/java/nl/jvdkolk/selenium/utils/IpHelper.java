package nl.jvdkolk.selenium.utils;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public final class IpHelper {

    public static String getPublicApplicationIp() {
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            return socket.getLocalAddress().getHostAddress();
        } catch (final SocketException | UnknownHostException e) {
            throw new IllegalArgumentException("Could not find ip where the application is running on", e);
        }
    }
}
